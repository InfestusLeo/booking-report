import numpy as np
import pandas as pd
from sqlalchemy import create_engine
import psycopg2
import json
import csv
import pygsheets as pg
from datetime import datetime

import sys
from pyhive import presto
# from pyhive import hive


def createPrestoConnection():
    try:
        conn = presto.Connection(host="presto.oyorooms.io", port=8889, username='weddingz.analyst@oyorooms.com')
        return conn
    except Exception as error:
        print("Unable to connect to Database")
        sys.exit(0)


presto_con = createPrestoConnection()

print("Opened database Successfully")

cur = presto_con.cursor()
#%%
df = pd.DataFrame()
df = pd.read_sql_query("""

SELECT
    ROW_NUMBER() OVER (PARTITION BY orders.orderid ORDER BY FROM_UNIXTIME((updateat+19800000)/1000) DESC, orders.publishedat DESC) AS ROW_NUM,
    FROM_UNIXTIME((createdat+19800000)/1000) AS Booking_date,
    FROM_UNIXTIME((updateat+19800000)/1000) AS Updated_date,
    FROM_UNIXTIME((startdate+19800000)/1000) AS Event_date,
    orders.orderid,
    weddingzleadid,
    orders.status,
    eventslots,
    eventtype,
    leadsource,
    totalamount AS GMV,
    markupamount as Markup,
    roomisoyobooking,
    roomprice,
    oyo_id,

    CASE roomisoyobooking
            /*WHEN TRUE THEN totalamount-roomprice*/
            WHEN TRUE THEN totalamount
            ELSE totalamount
        END AS FINALGMV,

    CASE
        WHEN orders.propertytype = 'WEDDINGZ' THEN 'WEDDINGZ'
        WHEN orders.propertytype = 'OTHER' AND oyohotel.category = 5 THEN 'SOB'
        WHEN oyohotel.category = NULL THEN 'WEDDINGZ'
        ELSE 'SMART'
    END AS FLAG

        FROM weddingz.weddingz_orders AS orders

        LEFT JOIN supply_onboarding_service.property as oyohotel
            ON CAST(oyohotel.property_id AS varchar) = CAST(orders.oyohotelid AS varchar)

    WHERE
        leadsource != 'MIGRATION' AND
        leadsource != 'Migration'

    ORDER BY
        Updated_date DESC,
        orders.year DESC,
        orders.month DESC,
        orders.day DESC,
        orders.hour DESC

""", presto_con)
print("Operation Done")

#%%
df_vendor = pd.DataFrame()
df_vendor = pd.read_sql_query("""

SELECT * FROM(
    SELECT
        ROW_NUMBER() OVER (PARTITION BY orderid, vendorbookingid ORDER BY publishedat DESC) ROW_NUM,
        orderid,
        vb.vendorBookingStatus as status,
        vb.bookingSellingPrice as sellingprice,
        CAST(vb.vendorShare AS double) as vendorshare
    
        FROM weddingz.weddingz_order_items as oi
        
        LEFT JOIN packages.vendorbookings_view as vb
        ON vb._id = oi.vendorbookingid
    ) as tabb
    
    WHERE tabb.status != 'CANCELLED' and ROW_NUM=1 --and
    --CAST(tabb.sellingprice as DOUBLE) - CAST(tabb.vendorshare as DOUBLE) != 0

""", presto_con)


df2 = pd.DataFrame()

# For Client, VM & Venue details
df2 = pd.read_sql_query("""

    SELECT
   leads.id AS weddingzleadid,
   leads.name as Customer_Name,
   vv.title AS venue_name,
   city.name AS City,
   users.name AS VM_Name,
   vv.payment_collection_upfront AS "Upfront Flag",
   vv.id as Venue_ID,
   users.email AS VM_Email,
   leads.phone_m AS Client_Phone
   FROM partyz_sqoop_staging.venue_crm_venueleads as leads
       LEFT JOIN partyz_sqoop_staging.venue_crm_customvenue AS cv
           ON leads.venue_id = cv.id
       LEFT JOIN weddingz.venue_venue as vv
           ON cv.venue_id = vv.id
       LEFT JOIN partyz_sqoop_staging.cities_city as city
           ON city.id = vv.city_id
       LEFT JOIN partyz_sqoop_staging.registration_user as users
           ON users.id = leads.venue_vm_id
   WHERE
       leads.service_status in ('Booking_Done','Booking Done')
   ORDER BY leads.created_date DESC

 """, presto_con)
presto_con.close()

#%%
df2 = df2.fillna("")

df = df[df['Booking_date'] >= '2019-12-01']
df = df[df['Booking_date'] < '2020-01-01']

df['weddingzleadid'] = df['weddingzleadid'].astype(int)
df2['weddingzleadid'] = df2['weddingzleadid'].astype(int)

df = pd.merge(df, df2, left_on='weddingzleadid', right_on='weddingzleadid', how='left')
#%%
df.loc[df['venue_name'] == 'Weddingz.in Banquet', 'FLAG'] = 'WSOB'
df.loc[df['venue_name'] == 'Hotel K P Inn', 'FLAG'] = 'WEDDINGZ'
df.loc[df['venue_name'] == 'Royal Palms Hotel', 'FLAG'] = 'WEDDINGZ'
df.loc[df['venue_name'] == 'Weddingz.in Banquet, Sector 3', 'FLAG'] = 'WSOB'
df.loc[df['oyo_id']=='GHY270','FLAG'] = 'Weddingz'
df.loc[df['oyo_id']=='AHM527','FLAG'] = 'SOB'

#%%
final = df[df['FLAG'] != 'SMART']
final = final[final['ROW_NUM'] == 1]
final = final[final['status'] != 'CANCELED']
final = final[final['status'] != 'FAILED']
#%%
# Calculating ER
df_vendor = df_vendor[df_vendor['ROW_NUM'] == 1]
df_vendor = df_vendor[['ROW_NUM', 'orderid', 'sellingprice', 'vendorshare']]
df_vendor = df_vendor.merge(df, left_on='orderid', right_on='orderid', how='left')
df_vendor['ER'] = df_vendor.sellingprice.astype(float) - df_vendor.vendorshare.astype(float)
#%%

####
df_vendor = df_vendor[['ROW_NUM_x','orderid','sellingprice','vendorshare','ER','FLAG']]
df_vendor_sob = df_vendor
####
#%%
df_vendor_sob[(df_vendor_sob['FLAG']=='SOB') | (df_vendor_sob['FLAG']=='WEDDINGZ')]

df_vendor.drop_duplicates(keep='first', inplace=True)
#%%
    
#%%
df_vendor = df_vendor[df_vendor['ER']>=0]
df_vendor = df_vendor[['orderid', 'sellingprice', 'vendorshare', 'ER']].groupby('orderid').sum()
#%%
final_non_sob = final[final['FLAG']!='SOB'].merge(df_vendor, left_on='orderid', right_on='orderid', how='left')
final_non_sob = final_non_sob[final_non_sob['ER'] >= 0]
final_non_sob = final_non_sob[final_non_sob['venue_name'] != 'CRM test venue']
#%%
final_sob = final[final['FLAG']=='SOB'].merge(df_vendor, left_on='orderid', right_on='orderid', how='left')
final_sob = final_sob[final_sob['venue_name'] != 'CRM test venue']

final = final_non_sob
final = final.append(final_sob)

#%%
# final.loc[final['City'] == 'Gurgaon', 'City'] = 'Delhi'
# final.loc[final['City'] == 'Ghaziabad', 'City'] = 'Delhi'
# final.loc[final['City'] == 'Meerut', 'City'] = 'Delhi'
# final.loc[final['City'] == 'Noida', 'City'] = 'Delhi'
#%%

def compute_NRV(final):
    if (final['FLAG'] == 'SOB' or final['leadsource'] == "MM"):
#        return (final['FINALGMV'] + final['Markup'])
        return final['FINALGMV']
    else:
#        return int((final['FINALGMV'] + final['Markup'])*1.18)
        return int(final['FINALGMV']*1.18)
#
#
final['NRV'] = final.apply(compute_NRV, axis=1)
#%%

final = final[final['ER'] != ""]

def compute_take(final):
    if final['FLAG'] == 'SOB':
        return int(final['NRV']*0.1*1.18)
    elif final['FLAG']=='WSOB':    
        return int(final['NRV']*0.1)
    elif final['leadsource'] == "MM":
#        return (final['ER'] + final['Markup'])
        return final['ER']
    else:
#        return int((final['ER'] + final['Markup'])*1.18)
        return int(final['ER']*1.18)

#%%
final = final.fillna("")



#%%
final['Take'] = final.apply(compute_take, axis=1)
#%%
def compute_finaltake(final):
    if (final['roomisoyobooking'] == True and final['FLAG']!='SOB'):
        return (final['Take'] + (final['roomprice']*0.1))
    else: return final['Take']

final['Take'] = final.apply(compute_finaltake, axis=1)

#%%
##NEED TO WORK ON AUTOMATING THIS
def compute_city(final):
    if ("AHM" in final['oyo_id']):
        return "Ahmedabad"
    elif (final['City'] == "Digha"):
        return "Kolkata"
    elif ("SRT" in final['oyo_id']):
        return "Surat"
    elif ("HYD" in final['oyo_id'] ):
        return "Hyderabad"
    elif ("JAI" in final['oyo_id']):
        return "Jaipur"
    elif ("NAG" in final['oyo_id'] ):
        return "Nagpur"
    elif ( "GRG" in final['oyo_id']):
        return "Gurugram"
    elif ( "IND" in final['oyo_id']):
        return "Indore"
    elif ( "DEL" in final['oyo_id']):
        return "Delhi"
    elif ( "KOL" in final['oyo_id']):
        return "Kolkata"
    elif ( "PUN" in final['oyo_id']):
        return "Pune"
    elif ( "VAD" in final['oyo_id']):
        return "Baroda"
    elif ( "BHO" in final['oyo_id']):
        return "Bhopal"
    elif ( "MUM" in final['oyo_id']):
        return "Mumbai"
    elif ( "BLR" in final['oyo_id']):
        return "Bangalore"
    elif ( "VNS" in final['oyo_id']):
        return "Varanasi"
    elif ( "GHY" in final['oyo_id']):
        return "Guwahati"
    elif ( "NOD" in final['oyo_id']):
        return "Noida"
    elif ( "SRG" in final['oyo_id']):
        return "Siliguri"
    elif ( "SHM" in final['oyo_id']):
        return "Shimla"
    elif ( "KOC" in final['oyo_id']):
        return "Kochi"
    elif ( "JMD" in final['oyo_id']):
        return "Jamshedpur"
    elif ( "BBS" in final['oyo_id']):
        return "Bhubaneswar"
    elif ( "LCK" in final['oyo_id']):
        return "Lucknow"
    else: 
        return final['City']


final['City'] = final.apply(compute_city, axis=1)

# Adding Take rate in the dataframe
final['Take Rate'] = final['Take']/final['NRV']
#removing the booking id
#final=final[final['orderid']!='5d925aa82183a20cc94f4924']
#%%
# Publishing the dataset to the google sheet

gc = pg.authorize(service_file='client_secret.json')
sh = gc.open('Booking report 2019')
wks = sh.worksheet_by_title('November')
wks.clear((1, 1))
wks.set_dataframe(final, (1, 1))

print("Done at {}".format(datetime.now()))
